var gulp = require('gulp')
var autoImports = require('gulp-auto-imports')
//var runSequence = require('run-sequence'); // попробовала
var autoprefixer = require('gulp-autoprefixer');
 
// gulp-auto-imports для автоимпортов scss файлов
gulp.task('imports', function () {
  var destination = './src/scss'
  return (
    gulp
      .src('./src/components/*.scss')
      .pipe(autoImports({ preset: 'scss', dest: destination }))
      .pipe(gulp.dest(destination))
  )
})

// для автодописывания префиксов
gulp.task('autoprefix', function(done){
    gulp.src('./src/components/**/*.scss').pipe(autoprefixer({overrideBrowserslist: ['last 2 versions']}))
    .pipe(gulp.dest('./src/scss'))
    done();
})

// для динамического автодописывания префиксов
gulp.task('watch', function (done) {
    gulp.watch('./src/components/*.scss', gulp.series('imports'))
    done();
})

// запуск всех тасков
gulp.task('default', gulp.series('autoprefix', 'watch'),function(done){
    done();
})